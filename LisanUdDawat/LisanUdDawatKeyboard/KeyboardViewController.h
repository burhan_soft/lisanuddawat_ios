//
//  KeyboardViewController.h
//  LisanUdDawatKeyboard
//
//  Created by Ranjeet Singh on 21/04/15.
//  Copyright (c) 2015 Burhan Soft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KeyboardViewController : UIInputViewController

@property (nonatomic) CGFloat portraitHeight;
@property (nonatomic) CGFloat landscapeHeight;
@property (nonatomic) BOOL isLandscape;
@property (nonatomic) NSLayoutConstraint *heightConstraint;

-(void) keyPressed : (UIButton *) button;

@end
