//
//  KeyboardViewController.h
//  LisanUdDawatKeyboard
//
//  Created by Ranjeet Singh on 21/04/15.
//  Copyright (c) 2015 Burhan Soft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KeyboardViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface LisanUdDawatKeyboard : UIView <UIInputViewAudioFeedback>

@property (strong, nonatomic) IBOutlet UIImageView *keyboardBackground;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *characterKeys;
@property (strong, nonatomic) IBOutlet UIButton *shiftButton;
@property (strong, nonatomic) IBOutlet UIButton *altButton;
@property (strong, nonatomic) IBOutlet UIButton *returnButton;
@property (strong, nonatomic) IBOutlet UIButton *deleteButton;
@property (strong) id<UITextInput> textView;
@property (strong, nonatomic) IBOutlet UIButton *spaceButton;

@property (strong, nonatomic) IBOutlet UIButton *suggestion_1;
@property (strong, nonatomic) IBOutlet UIButton *suggestion_2;
@property (strong, nonatomic) IBOutlet UIButton *suggestion_3;

- (id)init : (KeyboardViewController *) keyboardController;
- (IBAction)returnPressed:(id)sender;
- (IBAction)characterPressed:(id)sender;
- (IBAction)shiftPressed:(id)sender;
- (IBAction)altPressed:(id)sender;
- (IBAction)deletePressed:(id)sender;
- (IBAction)unShift;
- (IBAction)spacePressed:(id)sender;

@end
