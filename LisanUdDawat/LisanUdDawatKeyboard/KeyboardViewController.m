//
//  KeyboardViewController.m
//  LisanUdDawatKeyboard
//
//  Created by Ranjeet Singh on 21/04/15.
//  Copyright (c) 2015 Burhan Soft. All rights reserved.
//

#import "KeyboardViewController.h"
#import "LisanUdDawatKeyboard.h"
#import "WorldListDAO.h"

@interface KeyboardViewController ()
{
    NSMutableString *bufferString;
}
@property (nonatomic, strong) UIButton *nextKeyboardButton;
@property (nonatomic, strong) LisanUdDawatKeyboard *keyboard;
@end

@implementation KeyboardViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Perform custom initialization work here
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.keyboard = [[LisanUdDawatKeyboard alloc] init : self];
    bufferString = [NSMutableString new];
    [self registerActions];
    self.portraitHeight = 250;
    self.landscapeHeight = 250;
}

-(void)viewDidAppear:(BOOL)animated
{
    [self updateViewConstraints];
}

- (void)updateViewConstraints {
    [super updateViewConstraints];
    // Add custom view sizing constraints here
    
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    self.isLandscape = UIDeviceOrientationIsLandscape(orientation);
    CGFloat _expandedHeight = 0;
    if (self.isLandscape) {
        _expandedHeight = self.landscapeHeight;
    } else {
        _expandedHeight = self.portraitHeight;
    }
    
    self.heightConstraint =
    [NSLayoutConstraint constraintWithItem: self.view
                                 attribute: NSLayoutAttributeHeight
                                 relatedBy: NSLayoutRelationEqual
                                    toItem: nil
                                 attribute: NSLayoutAttributeNotAnAttribute
                                multiplier: 0.0
                                  constant: _expandedHeight];
    [self.view addConstraint: _heightConstraint];
    self.inputView = self.keyboard;
}

-(void) registerActions
{
    [self.keyboard.deleteButton addTarget:self action:@selector(deletePressed) forControlEvents:UIControlEventTouchUpInside];
    [self.keyboard.spaceButton addTarget:self action:@selector(spacePressed) forControlEvents:UIControlEventTouchUpInside];
    [self.keyboard.returnButton addTarget:self action:@selector(returnPressed) forControlEvents:UIControlEventTouchUpInside];
    
    [self.keyboard.suggestion_1 addTarget:self action:@selector(suggestionPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.keyboard.suggestion_2 addTarget:self action:@selector(suggestionPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.keyboard.suggestion_3 addTarget:self action:@selector(suggestionPressed:) forControlEvents:UIControlEventTouchUpInside];
}

-(void) suggestionPressed : (UIButton *) suggestionText
{
    [bufferString setString:@""];
    if([suggestionText currentTitle] != nil && ![[suggestionText currentTitle] isEqualToString: @""])
        [self.textDocumentProxy insertText:[NSString stringWithFormat:@" %@ ", [suggestionText currentTitle]]];
}

-(void) deletePressed
{
    [self.textDocumentProxy deleteBackward];
    if(bufferString.length > 0)
    {
        [bufferString deleteCharactersInRange:NSMakeRange(bufferString.length - 1, 1)];
        
    }
    if(bufferString.length == 0)
        [self emptyStrings];
    else
        [self setSuggestions:bufferString];
}

-(void) spacePressed
{
    [self.textDocumentProxy insertText:@" "];
    [bufferString setString:@""];
}

-(void) returnPressed
{
    [self.textDocumentProxy insertText:@"\n"];
    [bufferString setString:@""];
}

-(void) keyPressed : (UIButton*) key
{
    [self.textDocumentProxy insertText:[key currentTitle]];
    [bufferString appendString:[key currentTitle]];
    [self setSuggestions:bufferString];
}

-(void) emptyStrings
{
    [self.keyboard.suggestion_1 setTitle:@"" forState:UIControlStateNormal];
    [self.keyboard.suggestion_2 setTitle:@"" forState:UIControlStateNormal];
    [self.keyboard.suggestion_3 setTitle:@"" forState:UIControlStateNormal];
}

-(void) setSuggestions : (NSString *) searchString
{
    NSMutableArray * suggestions = [[WorldListDAO sharedManager] getSuggestions:searchString];
    if(nil != suggestions && [suggestions count] > 0)
    {
        for (int counter = 0; counter < [suggestions count]; counter++) {
            switch (counter) {
                case 0:
                    [self.keyboard.suggestion_2 setTitle:[suggestions objectAtIndex:counter] forState:UIControlStateNormal];
                    break;
                case 1:
                    [self.keyboard.suggestion_1 setTitle:[suggestions objectAtIndex:counter] forState:UIControlStateNormal];
                    break;
                case 2:
                    [self.keyboard.suggestion_3 setTitle:[suggestions objectAtIndex:counter] forState:UIControlStateNormal];
                    break;
                    
                default:
                    
                    break;
            }
        }
    }
    else
    {
        [self.keyboard.suggestion_2 setTitle:searchString forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated
}

- (void)textWillChange:(id<UITextInput>)textInput {
    // The app is about to change the document's contents. Perform any preparation here.
}

- (void)textDidChange:(id<UITextInput>)textInput {
    // The app has just changed the document's contents, the document context has been updated.
    
    UIColor *textColor = nil;
    if (self.textDocumentProxy.keyboardAppearance == UIKeyboardAppearanceDark) {
        textColor = [UIColor whiteColor];
    } else {
        textColor = [UIColor blackColor];
    }
    [self.nextKeyboardButton setTitleColor:textColor forState:UIControlStateNormal];
}

@end
