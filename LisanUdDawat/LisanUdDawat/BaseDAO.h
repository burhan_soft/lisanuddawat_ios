//
//  ViewController.h
//  LisanUdDawat
//
//  Created by Ranjeet Singh on 21/04/15.
//  Copyright (c) 2015 Burhan Soft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

extern NSString *const DB_NAME;

@interface BaseDAO : NSObject {
    
    sqlite3 *DBConn;
    
    NSString *DBPath;
}

-(sqlite3 *) getConnection;

@end
