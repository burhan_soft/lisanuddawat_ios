//
//  WorldListDAO.m
//  LisanUdDawat
//
//  Created by Ranjeet Singh on 22/04/15.
//  Copyright (c) 2015 Burhan Soft. All rights reserved.
//

#import "WorldListDAO.h"

@implementation WorldListDAO


+ (id)sharedManager {
    static WorldListDAO *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}


-(NSMutableArray*) getSuggestions : (NSString *) searchString
{
    sqlite3 *dbConn = [self getConnection];
    
    NSString *queryString = [NSString stringWithFormat:@"SELECT _id, word_text FROM tblburhan where word_text LIKE '%@%%'", searchString];
    
    const char *sqlStatement = [queryString UTF8String];
    sqlite3_stmt *selectTransactionStmt = nil;
    int connResult = sqlite3_prepare_v2(dbConn, sqlStatement, -1, &selectTransactionStmt, NULL);
    NSMutableArray *suggestionArray = [NSMutableArray new];
    int resultCount = 0;
    if(connResult == SQLITE_OK) {
        
        while(sqlite3_step(selectTransactionStmt) == SQLITE_ROW && resultCount != 3)
        {
            sqlite3_column_int(selectTransactionStmt, 0);
            [suggestionArray addObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectTransactionStmt, 1)]];
            resultCount++;
        }
    }
    sqlite3_finalize(selectTransactionStmt);
    sqlite3_close(dbConn);
    return suggestionArray;
}

@end
