//
//  WorldListDAO.h
//  LisanUdDawat
//
//  Created by Ranjeet Singh on 22/04/15.
//  Copyright (c) 2015 Burhan Soft. All rights reserved.
//

#import "BaseDAO.h"

@interface WorldListDAO : BaseDAO

+ (id)sharedManager;

-(NSMutableArray*) getSuggestions : (NSString *) searchString;

@end
