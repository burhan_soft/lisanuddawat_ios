//
//  AppDelegate.h
//  LisanUdDawat
//
//  Created by Ranjeet Singh on 21/04/15.
//  Copyright (c) 2015 Burhan Soft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "KeyboardViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property(nonatomic, retain) KeyboardViewController *keyboardController;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end

