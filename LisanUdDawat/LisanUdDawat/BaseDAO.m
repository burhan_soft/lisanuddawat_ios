//
//  ViewController.h
//  LisanUdDawat
//
//  Created by Ranjeet Singh on 21/04/15.
//  Copyright (c) 2015 Burhan Soft. All rights reserved.
//

#import "BaseDAO.h"

NSString *const DB_NAME = @"database_new.sqlite";


@interface BaseDAO()

-(void) initializeDBConn;
-(bool) checkDatabase:(NSString *)dbPath;
-(void) copyDatabase:(NSString *)dbName dbPath:(NSString *)dbPath;

@end

@implementation BaseDAO

-(id) init {
    self = [super init];
    [self initializeDBConn];
    return self;
}

-(sqlite3 *) getConnection {
    [self initializeDBConn];
    return DBConn;
}

-(void) initializeDBConn {
    // Get the path to the documents directory and append the databaseName
	NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
	NSString *documentsDir = [documentPaths objectAtIndex:0];
    DBPath = [documentsDir stringByAppendingPathComponent:DB_NAME];
    
    if ([self checkDatabase:DBPath] == FALSE) {
        [self copyDatabase:DB_NAME dbPath:DBPath];
    }
    
    if(sqlite3_open([DBPath UTF8String], &DBConn) == SQLITE_OK)
    {
        NSLog(@"OPEN");
    }
}

// Check and create the Databse.
-(bool) checkDatabase:(NSString *)dbPath {
	// Check if the SQL database has already been saved to the users phone, if not then copy it over
	BOOL success = FALSE;
	// Create a FileManager object, we will use this to check the status
	// of the database and to copy it over if required
	NSFileManager *fileManager = [NSFileManager defaultManager];
	// Check if the database has already been created in the users filesystem
	success = [fileManager fileExistsAtPath:dbPath];
	// If the database already exists then return without doing anything
	return success;
}

//Copy the Database at database path
-(void) copyDatabase:(NSString *)dbName dbPath:(NSString *)dbPath {
	
	NSFileManager *fileManager = [NSFileManager defaultManager];
    
	// Get the path to the database in the application package
	NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] 
									 stringByAppendingPathComponent:dbName];
	
	// Copy the database from the package to the users filesystem
	[fileManager copyItemAtPath:databasePathFromApp toPath:dbPath error:nil];
}

@end
